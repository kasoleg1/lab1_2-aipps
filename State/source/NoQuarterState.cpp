#include "../include/NoQuarterState.h"

NoQuarterState::NoQuarterState(GumballMachine* _gumballMachine)
{
	gumballMachine = _gumballMachine;
}
	
void NoQuarterState::insertQuarter()
{
	cout << "You inserted a quarter" << endl;
	gumballMachine->setState(gumballMachine->getHasQuarterState());
}
	
void NoQuarterState::ejectQuarter()
{
	cout << "You haven't inserted a quarter" << endl;
}

void NoQuarterState::turnCrank()
{
	cout << "You turned, but there's no quarter" << endl;
}
	
void NoQuarterState::dispense()
{
	cout << "You need to pay first" << endl;
}
