#include "../include/HasQuarterState.h"

HasQuarterState::HasQuarterState(GumballMachine* _gumballMachine)
{
	gumballMachine = _gumballMachine;
}
	
void HasQuarterState::insertQuarter()
{
	cout << "You can't insert another quarter" << endl;
}
	
void HasQuarterState::ejectQuarter()
{
	cout << "Quarter returned" << endl;
	gumballMachine->setState(gumballMachine->getNoQuarterState());
}
	
void HasQuarterState::turnCrank()
{
	cout << "You turned..." << endl;
	gumballMachine->setState(gumballMachine->getSoldState());
}
	
void HasQuarterState::dispense()
{
	cout << "No gumball dispensed" << endl;
}
