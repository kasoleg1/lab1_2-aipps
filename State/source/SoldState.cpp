#include "../include/SoldState.h"

SoldState::SoldState(GumballMachine* _gumballMachine)
{
	gumballMachine = _gumballMachine;
}

void SoldState::insertQuarter()
{
	cout << "Please wait, we're already giving you a gumball" << endl;
}
	
void SoldState::ejectQuarter()
{
	cout << "Sorry, you already turned the crank" << endl;
}
	
void SoldState::turnCrank()
{
	cout << "Turnung twice doesn't get you another gumball!" << endl;
}
	
void SoldState::dispense()
{
	gumballMachine->releaseBall();
	if (gumballMachine->getCount() > 0)
		gumballMachine->setState(gumballMachine->getSoldOutState());
	else
	{
		cout << "Oops, out of gumballs!" << endl;
		gumballMachine->setState(gumballMachine->getSoldOutState());
	}
}
