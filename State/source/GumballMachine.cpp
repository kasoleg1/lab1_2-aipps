#include "../include/GumballMachine.h"

GumballMachine::GumballMachine(int numberGumballs)
{
	//state = soldOutState;
	soldOutState = (State*)(new SoldOutState(this));
	noQuarterState = (State*)(new NoQuarterState(this));
	hasQuarterState = (State*)(new HasQuarterState(this));
	soldState = (State*)(new SoldState(this));
	count = numberGumballs;
	if (numberGumballs > 0)
		state = noQuarterState;
}
	
void GumballMachine::insertQuarter()
{
	state->insertQuarter();
}

void GumballMachine::ejectQuarter()
{
	state->ejectQuarter();
}
	
void GumballMachine::turnCrank()
{
	state->turnCrank();
	state->dispense();
}

State* GumballMachine::getSoldOutState()
{
	return soldOutState;
}
	
State* GumballMachine::getNoQuarterState()
{
	return noQuarterState;
}
	
State* GumballMachine::getHasQuarterState()
{
	return hasQuarterState;
}
	
State* GumballMachine::getSoldState()
{
	return soldState;
}

void GumballMachine::setState(State* _state)
{
	state = _state;
}
	
void GumballMachine::releaseBall()
{
	cout << "A gumball comes rolling out the slot..." << endl;
	if (count != 0)
		count--;
}

int GumballMachine::getCount()
{
	return count;
}
