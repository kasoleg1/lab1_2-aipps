#include "../include/SoldOutState.h"

SoldOutState::SoldOutState(GumballMachine* _gumballMachine)
{
	gumballMachine = _gumballMachine;
}
	
void SoldOutState::insertQuarter()
{
	cout << "Tou can't insert a quarter, the machine is sold out" << endl;
}
	
void SoldOutState::ejectQuarter()
{
	cout << "You can't eject, you haven't inserted a quarter yet" << endl;
}
	
void SoldOutState::turnCrank()
{
	cout << "You turned, but there are not gumballs" << endl;
}
	
void SoldOutState::dispense()
{
	cout << "No gumball dispensed" << endl;
}
