#pragma once
#include <iostream>
#include "State.h"
#include "GumballMachine.h"
using namespace std;

class GumballMachine;
class HasQuarterState: State
{
private:
	GumballMachine* gumballMachine;
public:
	HasQuarterState(){}
	HasQuarterState(GumballMachine* _gumballMachine);
	void insertQuarter();
	void ejectQuarter();
	void turnCrank();
	void dispense();
};