#pragma once
#include <iostream>
#include "State.h"
#include "GumballMachine.h"
using namespace std;

class GumballMachine;
class NoQuarterState: State
{
private:
	GumballMachine* gumballMachine;

public:
	NoQuarterState(){}
	NoQuarterState(GumballMachine* gumballMachine);
	void insertQuarter();
	void ejectQuarter();
	void turnCrank();
	void dispense();
};