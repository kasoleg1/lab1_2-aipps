#pragma once
#include "State.h"
#include "HasQuarterState.h"
#include "NoQuarterState.h"
#include "SoldOutState.h"
#include "SoldState.h"

class GumballMachine
{
private:
	State* soldOutState;
	State* noQuarterState;
	State* hasQuarterState;
	State* soldState;

	//State* state = soldOutState;
	State* state;
	int count;

public:
	GumballMachine(){}
	GumballMachine(int numberGumballs);
	void insertQuarter();
	void ejectQuarter();
	void turnCrank();
	State* getSoldOutState();
	State* getNoQuarterState();
	State* getHasQuarterState();
	State* getSoldState();
	void setState(State* _state);
	void releaseBall();
	int getCount();
};