#pragma once
#include <iostream>
#include "State.h"
#include "GumballMachine.h"
using namespace std;

class GumballMachine;
class SoldState: State
{
private:
	GumballMachine* gumballMachine;
public:
	SoldState(){}
	SoldState(GumballMachine* _gumballMachine);
	void insertQuarter();
	void ejectQuarter();
	void turnCrank();
	void dispense();
};