#pragma once
#include <iostream>
#include "State.h"
#include "GumballMachine.h"
using namespace std;

class GumballMachine;
class SoldOutState: State
{
private:
	GumballMachine* gumballMachine;
public:
	SoldOutState(){}
	SoldOutState(GumballMachine* _gumballMachine);
	void insertQuarter();
	void ejectQuarter();
	void turnCrank();
	void dispense();
};
