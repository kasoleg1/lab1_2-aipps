#pragma once
#include "Tuner.h"
#include "DvdPlayer.h"
#include "CdPlayer.h"

class Tuner;
class DvdPlayer;
class CdPlayer;
class Amplifier
{
private:
	Tuner* tuner;
	DvdPlayer* dvdPlayer;
	CdPlayer* cdPlayer;

public:
	void on();
	void off();
	void setCd();
	void setDvd();
	void setStereoSound();
	void setSurroundSound();
	void setTuner();
	void setVolume();
	//void toString();
};