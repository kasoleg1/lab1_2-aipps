#pragma once
#include <iostream>
#include <string>
#include "DvdPlayer.h"
using namespace std;

class Projector
{
public:
	void on();
	void off();
	void tvModel(string model);
	void wideScreenMode();
};