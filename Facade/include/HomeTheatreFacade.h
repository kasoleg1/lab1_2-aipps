#pragma once
#include "CdPlayer.h"
#include "DvdPlayer.h"
#include "PopcornPopper.h"
#include "Projector.h"
#include "Screen.h"
#include "TheatreLights.h"
#include "Tuner.h"
#include "Amplifier.h"

class HomeTheatreFacade
{
public:
	void watchMovie();
	void endMovie();
	void listenToCd();
	void endCd();
	void listenToRadio();
	void endRadio();
};