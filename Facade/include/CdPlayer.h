#pragma once
#include "Amplifier.h"

class Amplifier;
class CdPlayer
{
private:
	Amplifier* amplifier;

public:
	void on();
	void off();
	void eject();
	void pause();
	void play();
	void stop();
};