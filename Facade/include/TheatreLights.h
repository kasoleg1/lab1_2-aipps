#pragma once
#include <iostream>
using namespace std;

class TheatreLights
{
public:
	void on();
	void off();
	void dim(int dim);
};