#pragma once
#include <iostream>
using namespace std;

class PopcornPopper
{
public:
	void on();
	void off();
	void pop();
};