#pragma once
#include "Amplifier.h"

class Amplifier;
class Tuner
{
private:
	Amplifier* amplifier;

public:
	void on();
	void off();
	void setAm();
	void setFm();
	void setFrequency();
};