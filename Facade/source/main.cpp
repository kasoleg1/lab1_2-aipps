#include <iostream>
#include "../include/HomeTheatreFacade.h"
#include "../State/include/GumballMachine.h"
using namespace std;


int main()
{
	
	HomeTheatreFacade* homeTheatre = new HomeTheatreFacade();
	GumballMachine* gum = new GumballMachine(10);
	
	cout <<"--------------------------------------------" << endl;
	homeTheatre->watchMovie();
	//system("pause");
	cout << endl;
	homeTheatre->endMovie();
	cout <<"--------------------------------------------" << endl;
	gum->insertQuarter();
	gum->turnCrank();
	cout <<"--------------------------------------------" << endl;

	delete(homeTheatre);
	delete(gum);
	return 0;
}
