#include "../include/PopcornPopper.h"

void PopcornPopper::on()
{
	cout << "Popper is turned" << endl;
}
	
void PopcornPopper::off()
{
	cout << "Popper is stopped" << endl;
}
	
void PopcornPopper::pop()
{
	cout << "Popcorn ready" << endl;
}
