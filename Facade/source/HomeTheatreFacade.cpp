#include "../include/HomeTheatreFacade.h"

PopcornPopper* popper;
Projector* projector;
TheatreLights* lights;
Screen* screen;

void HomeTheatreFacade::watchMovie()
{
	popper = new PopcornPopper();
	projector = new Projector();
	lights = new TheatreLights();
	screen = new Screen();

	//�������� ������� ��� ��������
	popper->on();
	//������ ��������
	popper->pop();
	//���������� ������� �����
	lights->dim(10);
	//�������� �����
	screen->down();
	//�������� ��������
	projector->on();
	//���������� dvd
	projector->tvModel("dvd");
	//��������������� ��������
	projector->wideScreenMode();

	cout << "Pleasant viewing" << endl;
}

void HomeTheatreFacade::endMovie()
{
	//��������� ��������
	projector->off();
	//��������� �����
	screen->up();
	//��������� ������� ��� ��������
	popper->off();
	//��������� ���������
	lights->off();

	delete(popper);
	delete(projector);
	delete(lights);
	delete(screen);
}
	
void HomeTheatreFacade::listenToCd()
{

}

void HomeTheatreFacade::endCd()
{

}

void HomeTheatreFacade::listenToRadio()
{

}

void HomeTheatreFacade::endRadio()
{

}
