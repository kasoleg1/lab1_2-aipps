#include "../include/Projector.h"

void Projector::on()
{
	cout << "Projector on" << endl;
}
	
void Projector::off()
{
	cout << "Projector off" << endl;
}
	
void Projector::tvModel(string model)
{
	if (model == "dvd")
		cout << "Dvd is set" << endl;
}
	
void Projector::wideScreenMode()
{
	cout << "Projector in Wide Screen Mode" << endl;
}
