#include "../include/TheatreLights.h"

void TheatreLights::on()
{
	cout << "Light on" << endl;
}
	
void TheatreLights::off()
{
	cout << "Light off" << endl;
}
	
void TheatreLights::dim(int dim)
{
	cout << "Light is " << dim << endl;
}
